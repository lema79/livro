resource "datadog_monitor" "cpu_anomalous" {
  name    = "Anomalous CPU usage"
  type    = "metric alert"
  message = "CPU utilization is outside normal bounds"
  query   = "avg(last_1m):avg:system.cpu.system{*} by {host} > 60"
}