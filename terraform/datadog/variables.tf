variable "datadog_api_key" {
  type = string
}

variable "datadog_app_key" {
  type = string
}

variable "api_url" {
  #type = 
  default = "https://api.datadoghq.com"
}